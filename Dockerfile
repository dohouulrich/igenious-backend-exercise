# Get node image
FROM node:12.16.1-alpine

# Create app directory
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app

# Bundle app source
COPY . /usr/src/app

# Install packages
RUN npm ci

# Build application
RUN npm run build

# Expose Port
EXPOSE 3000

# Start Server
CMD ["npm", "run" , "start:prod"]
