/* eslint-disable @typescript-eslint/no-var-requires */
const https = require('https');
const fs = require('fs');
const path = require('path');

const downloadFile = () => {
  try {
    const xmlStoragePath = path.join(__dirname, 'dist', 'eurofxref.xml');
    const xmlStoragePathDev = path.join(__dirname, 'src', 'eurofxref.xml');

    const file = fs.createWriteStream(xmlStoragePath);
    const fileDev = fs.createWriteStream(xmlStoragePathDev);

    const xmlFileUrl = 'https://www.ecb.europa.eu/stats/eurofxref/eurofxref-hist-90d.xml';
  
    https.get(xmlFileUrl, function(response) {
      response.pipe(file);
      response.pipe(fileDev);
    });
  } catch (error) {
    console.error(error);
  }
}

downloadFile();