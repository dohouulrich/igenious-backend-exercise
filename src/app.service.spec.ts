import { Test } from '@nestjs/testing';
import { TestingModule } from '@nestjs/testing/testing-module';
import { AppService } from './app.service';

describe('App service', () => {
  let testingModule: TestingModule;
  let service: AppService;

  beforeEach(async () => {
    testingModule = await Test.createTestingModule({
      providers: [AppService],
    }).compile();

    service = testingModule.get(AppService);
  });

  describe('convert', () => {
    it('should return 90 when amount is "10" and rate is "90"', async () => {
      const result = await service.convert(10, 9);
      expect(result).toEqual(90);
    });

    it('should return NaN when rate is not a number', async () => {
      const result = await service.convert(10, NaN);
      expect(result).toEqual(NaN);
    });

    it('should return 0 when rate is not a number', async () => {
      const result = await service.convert(10, null);
      expect(result).toEqual(0);
    });
  });

  describe('currencyRateByDate', () => {
    it('should return expectedRateResponse when parameters are correct', async () => {
      const srcCurrency = 'USD';
      const expectedRateResponse = 1.0982;
      const conversionDate = '2020-03-17';

      const result = await service.currencyRateByDate(
        conversionDate,
        srcCurrency,
      );

      expect(result).toEqual(expectedRateResponse);
    });

    it('should return null when date not found', async () => {
      const srcCurrency = 'USD';
      const expectedAmountResponse = null;
      const conversionDate = '2019-03-17';

      const result = await service.currencyRateByDate(
        conversionDate,
        srcCurrency,
      );

      expect(result).toEqual(expectedAmountResponse);
    });
  });
});
