/* istanbul ignore next */

import { Controller, Get, HttpCode, Param } from '@nestjs/common';
import { AppService } from './app.service';
import { AppValidation } from './app.validation';
import { AppResponse } from './app.response';
import { Logger } from 'nestjs-pino';

@Controller()
export class AppController {
  constructor(
    private readonly appService: AppService,
    private logger: Logger,
  ) {}

  @Get('convert/:amount/:reference_date/:dest_currency/:src_currency')
  @HttpCode(200)
  convert(@Param() params: AppValidation): AppResponse {
    this.logger.log(
      `Request with parameters : ${JSON.stringify(params)}`,
      'Conversion request',
    );

    const amount = Number(params.amount);
    const destCurrency = params.dest_currency;
    const destRate = this.appService.currencyRateByDate(
      params.reference_date,
      destCurrency,
    );

    const response: AppResponse = new AppResponse();

    if (destRate === null) {
      this.logger.log(`Rate is NULL`, 'Conversion request');
      response.setAmount(null);
      response.setCurrency(null);
      return response;
    }

    const convertedAmount = this.appService.convert(amount, destRate);

    this.logger.log(
      `Converted amount is : ${convertedAmount}`,
      'Conversion request',
    );

    response.setAmount(convertedAmount);
    response.setCurrency(destCurrency);

    this.logger.log(
      `Response returned is : ${JSON.stringify(response)}`,
      'Conversion request',
    );

    return response;
  }
}
