import { IsNotEmpty, IsString, IsIn, Matches } from 'class-validator';
import { appConfig } from './app.config';

export class AppValidation {
  @IsNotEmpty()
  @IsString()
  amount: string;

  @IsNotEmpty()
  @Matches(/^\d{4}-\d{2}-\d{2}$/)
  reference_date: string;

  @IsNotEmpty()
  @IsString()
  @IsIn(appConfig.allowedCurrencies)
  dest_currency: string;

  @IsNotEmpty()
  @IsString()
  @IsIn(appConfig.allowedCurrencies)
  src_currency: string;
}
