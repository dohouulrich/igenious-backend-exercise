export class AppResponse {
  protected amount: number;

  protected currency: string;

  setAmount(amount: number) {
    this.amount = amount;
  }

  getAmount() {
    return this.amount;
  }

  setCurrency(currency: string) {
    this.currency = currency;
  }

  getCurrency() {
    return this.currency;
  }
}
