import { Injectable } from '@nestjs/common';
import * as convert from 'xml-js';
import { appConfig } from './app.config';
import { readFileSync } from 'fs';

@Injectable()
export class AppService {
  currencyRateByDate(targetDate: string, targetCurrency: string): number {
    const xmlContent = readFileSync(appConfig.xmlCurrencyPath, 'utf8');

    const options = {
      compact: true,
      spaces: 4,
      ignoreAttributes: false,
      ignoreCdata: true,
      ignoreText: true,
    };
    const result = convert.xml2js(xmlContent, options);
    const allCubes = result['gesmes:Envelope'].Cube.Cube;
    const targetCube = allCubes.find(
      cube => cube._attributes.time === targetDate,
    );

    if (
      targetCube &&
      targetCube.hasOwnProperty('Cube') &&
      targetCube.hasOwnProperty('_attributes') &&
      Array.isArray(targetCube.Cube)
    ) {
      const rate = targetCube['Cube'].find(
        cube => cube._attributes.currency === targetCurrency,
      );

      if (rate && rate.hasOwnProperty('_attributes')) {
        return Number(rate._attributes.rate);
      }
    }

    return null;
  }

  convert(amount: number, rate: number): number {
    return Number(amount) * Number(rate);
  }
}
