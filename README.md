# iGenious Backend Assignment

The application is build on top of NodeJs + TypeScript using NestJs Framework
Prerequisites and Good to have to run this application :

* [Install Node.js and Npm](https://nodejs.org)
* [Docker and Docker Compose](https://www.docker.com/)

## **Install the project**

* Clone the project with git or download the source code somewhere on your machine
* At the root of the project run `npm ci` or `npm install`

## **Run Unit tests and e2e Tests**

* For unit tests and to check tests coverage run : `npm run test:cov`. A coverage report can be viewed in the browser at this url file:///PATH_TO_SOURCE_CODE/coverage/lcov-report/index.html
* To run End-To-End tests you can run : `npm run test:e2e`

## **Run the project**

You can run this app directly with Node or with Docker. If you have Docker and Docker Compose installed I recommand you choose this option. You need to be at the root of the project

* With Node (For production): `npm run start:prod`
* With Node (For developement): `npm run start:dev` or `npm run start:dev`
* With Docker & Docker Compose (For production): `docker-compose up -d --build`

The app will be exposed on port `14000` if you run with Docker and `3000` if your run without it.

To test the app open your browser or postman and launch a GET request with this example of url : <http://localhost:3000/convert/amount/reference_date/dest_currency/src_currency>

> NOTICE : For this exercise the XML rate file in already based on EURO. For that we dont
> actually need the source currency. The file represents conversion from EURO to others. But
> source currency of still required in the URL but not actually used

## **TODO**

* ~~Add GitlabCI integration~~
* ~~Run Utest and e2e Tests on merge requests and on specific branches in CI~~
* Code analysis and security check in CI
* Deployement JOB in CI
