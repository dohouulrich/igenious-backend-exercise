import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication, ValidationPipe } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from './../src/app.module';

describe('AppController (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    app.useGlobalPipes(new ValidationPipe());
    await app.init();
  });

  it('(GET) /convert ReturnStatusCodeSuccessWithNullAmountAndNullCurrencyResponseWhenGetConvertRouteWithBadSrcCurrency', () => {
    return request(app.getHttpServer())
      .get('/convert/1/2020-03-17/BAD/GBP')
      .expect(200)
      .expect({ amount: null, currency: null });
  });

  it('(POST) /convert ReturnStatusCode404ErrorWhenGetConvertRouteWithPostMethod', () => {
    return request(app.getHttpServer())
      .post('/convert/1/2020-03-17/BAD/GBP')
      .expect(404);
  });

  it('(GET) /convert ReturnStatusCodeSuccessWithNonEmptyResponseWhenGetConvertRouteWithUsdAsSrcCurrency', () => {
    const srcCurrency = 'USD';
    const expectedAmountResponse = 1.0982;

    const expectedResponse = {
      amount: expectedAmountResponse,
      currency: srcCurrency,
    };

    return request(app.getHttpServer())
      .get('/convert/1/2020-03-17/USD/GBP')
      .expect(200)
      .expect(expectedResponse);
  });

  it('(GET) /convert ReturnStatusCode400ErrorWhenGetConvertRouteWithBadParameters', () => {
    return request(app.getHttpServer())
      .get('/convert/20/17-03-20/SOFT/SOFT')
      .expect(400);
  });

  afterAll(async () => {
    await app.close();
  });
});
